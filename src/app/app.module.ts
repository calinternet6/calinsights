import { BrowserModule } from '@angular/platform-browser';
import { NgModule,APP_INITIALIZER } from '@angular/core';
import { AppComponent } from './app.component';
import { InsightComponent } from './insight/insight.component';
import { InsightService } from './services/insight.service';
import { UserService } from './services/user.service';
import {GetResourcesService}from './services/get-resources.service';
import { ChartjsModule } from '@ctrl/ngx-chartjs';
import { DataGridComponent } from './data-grid/data-grid.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartComponent } from './chart/chart.component';
import { InterceptService} from './services/intercept.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LastPaymentComponent } from './last-payment/last-payment.component';
import { DidYouKnowComponent } from './did-you-know/did-you-know.component';
import { InsightFeedbackComponent } from './insight-feedback/insight-feedback.component';
import { InsightDateComponent } from './insight-date/insight-date.component';
import { NewDirectDebitComponent } from './new-direct-debit/new-direct-debit.component';
import { RefundComponent } from './refund/refund.component';
import { GetCashForeignCurrencyComponent } from './get-cash-foreign-currency/get-cash-foreign-currency.component';
import { ChargesForeignCurrencyComponent } from './charges-foreign-currency/charges-foreign-currency.component';
import { ModalContentComponent } from './modal-content/modal-content.component';
import { HttpClient } from '@angular/common/http';
import { ChartWithoutDigitsComponent } from './chart-without-digits/chart-without-digits.component'; 
import { getRandomString } from 'selenium-webdriver/safari';

//export function init_app(getResourcesService: GetResourcesService, http: HttpClient) {
  
 // return new Promise((resolve, reject) => {
 //   this.http
 //       .get('https://stage-restservices.cal-online.co.il/Cal4U-Insights/CalInsights/api/GetInsightsMetaData')       
 //       .subscribe(response => {        
 //          console.log('END END END END')
  //          resolve(true);
   //     })
  //});

 // return () => this.http.get('https://stage-restservices.cal-online.co.il/Cal4U-Insights/CalInsights/api/GetInsightsMetaData')
 // .subscribe((data)=>{
 //   console.log(data)
 // })
 

  //return () => getResourcesService.getCoins().subscribe(data=>{
  //  getResourcesService.fillCoinsArr(data);
  //});    
//}

@NgModule({
  declarations: [
    AppComponent,
    InsightComponent,
    DataGridComponent,    
    ChartComponent,
    LastPaymentComponent,
    DidYouKnowComponent,    
    InsightFeedbackComponent,
    InsightDateComponent,
    NewDirectDebitComponent,    
    RefundComponent,
    GetCashForeignCurrencyComponent,
    ChargesForeignCurrencyComponent,    
    ModalContentComponent,
    ChartWithoutDigitsComponent
  ],
  imports: [
    BrowserModule,
    ChartjsModule,
    AngularFontAwesomeModule,
    BrowserAnimationsModule,
    HttpClientModule
    
  ],
  providers: [
    UserService,  
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true
    },
   
   // { provide: APP_INITIALIZER, useFactory: get_settings, deps: [AppLoadService], multi: true }
     GetResourcesService,
     InsightService,
     {
      provide: APP_INITIALIZER,   
      useFactory: (getResourcesService: GetResourcesService) => ()=>{        
        return getResourcesService.getMetaData().subscribe((data)=>{
          console.log("YESH DATA")
        getResourcesService.fillMetaData(data)  
        console.log("META DATA",data)
            return getResourcesService.loadCoins().subscribe((coins)=>{
              console.log("YESH COINS")   
              getResourcesService.fillCoinsArr(coins);           
            })      
      })},
      deps: [GetResourcesService],
      multi: true
    }],
  
  bootstrap: [AppComponent]
})
export class AppModule { }


  