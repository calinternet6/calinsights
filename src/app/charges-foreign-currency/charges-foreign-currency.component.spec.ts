import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargesForeignCurrencyComponent } from './charges-foreign-currency.component';

describe('ChargesForeignCurrencyComponent', () => {
  let component: ChargesForeignCurrencyComponent;
  let fixture: ComponentFixture<ChargesForeignCurrencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargesForeignCurrencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargesForeignCurrencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
