import { Component, OnInit,Input } from '@angular/core';
import { insight ,InsightTemplates} from  '../../models';
import { GetResourcesService } from '../services/get-resources.service';
import { InsightService } from '../services/insight.service';

@Component({
  selector: 'app-charges-foreign-currency',
  templateUrl: './charges-foreign-currency.component.html',
  styleUrls: ['./charges-foreign-currency.component.less'],
  
})
export class ChargesForeignCurrencyComponent implements OnInit {

  @Input() insight:insight;
  @Input() insightTemplates:InsightTemplates;
  public displayModal :boolean = false;
  constructor(public getResourcesService:GetResourcesService,public insightService:InsightService) { }
 
  ngOnInit() {
    
  }
  showModal(){
    
    this.displayModal = true;
    
  
  } 

}
