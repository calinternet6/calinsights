import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartWithoutDigitsComponent } from './chart-without-digits.component';

describe('ChartWithoutDigitsComponent', () => {
  let component: ChartWithoutDigitsComponent;
  let fixture: ComponentFixture<ChartWithoutDigitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartWithoutDigitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartWithoutDigitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
