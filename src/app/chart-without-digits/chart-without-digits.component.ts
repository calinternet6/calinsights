import { Component, OnInit ,Input} from '@angular/core';
import { insight, insightType, column,mock ,InsightTemplates} from '../../models';
import { InsightService } from '../services/insight.service';
import { ChartjsModule } from '@ctrl/ngx-chartjs';
import { tryParse } from 'selenium-webdriver/http';
import { GetResourcesService } from '../services/get-resources.service';

@Component({
  selector: 'app-chart-without-digits',
  templateUrl: './chart-without-digits.component.html',
  styleUrls: ['./chart-without-digits.component.less']
})
export class ChartWithoutDigitsComponent implements OnInit {

  @Input() insight:insight;
  @Input() insightTemplates:InsightTemplates;
  public config;
  public data;
  public columns:column[];
  public mock:mock[];
  constructor(public getResourcesService:GetResourcesService) {
    this.columns = [];
    this.mock = [];
   }

   ngOnInit() {
    //this.getChartData();
    this.buildCharts();
  }
  calcDivHeight(h:string)
  {    
    let divHeight = parseInt(h) + 30;
    return divHeight + 'px';
  }
  buildCharts()
  {   

 this.insight.InsightDetails.forEach(element => {
  var formattedMonth = ('0' + (new Date(new Date(element.TransactionDate.toString()).toLocaleDateString('en-GB')).getMonth() + 1)).slice(-2);
  var formattedYear = new Date(new Date(element.TransactionDate.toString()).toLocaleDateString('en-GB')).getFullYear().toString().substr(2,2);
       
 
      let c = new column();
      c.isBlue = true;
      c.date = element.TransactionDate;
      c.displayDate = formattedMonth +'/'+formattedYear;
      c.value = element.TransactionSum.toString();
      c.height = '0';
      c.currencyCode = element.CurrancyCode;
      this.columns.push(c);
    });
  
    this.columns.sort((a: any, b: any) => {
      if (a.date < b.date) {
        return -1;
      } else if (a.date > b.date) {
        return 1;
      } else {
        return 0;
      }
    });
  

    var fixHeight = 170;
    var divContainer
    
    var largest = Math.max.apply(Math, this.insight.InsightDetails.map((o)=> { return o.TransactionSum; }))

    // drew color in green
    for(let i=0;i< this.insight.InsightDetails.length;i++)
      {
          if(String(this.insight.InsightDetails[i].TransactionSum) == String(largest))
             {
              var result = this.columns.find(obj => {
                return String(obj.value) === String(largest)
                });
              
              result.isBlue = false;
             }
         
      }
     

    if(largest > fixHeight)
    {
      var currentHeight = largest / fixHeight;
      for(let i=0;i< this.insight.InsightDetails.length;i++)
      {
          var newHeight = Number(this.insight.InsightDetails[i].TransactionSum) / currentHeight;        
         var result = this.columns.find(obj => {
          return String(obj.date) ===  String(this.insight.InsightDetails[i].TransactionDate)
        })
        result.height = String(newHeight);
        
      }
     
    }
    else
    {

      var currentHeight = fixHeight / largest;
      for(let i=0;i< this.insight.InsightDetails.length;i++)
      {
        var newHeight = Number(this.insight.InsightDetails[i].TransactionSum)  * currentHeight;     
        var result = this.columns.find(obj => {
          return String(obj.date) ===  String(this.insight.InsightDetails[i].TransactionDate)
        })
        result.height = String(newHeight);
      }
    }  
  }       

}
