import { Component, OnInit, Input } from '@angular/core';
import { trigger, transition, animate, style } from '@angular/animations'
import { insight, insightType, InsightTemplates, MyDidYouKnow } from '../../models';
import { InsightService } from '../services/insight.service';
import { GetResourcesService } from '../services/get-resources.service';

@Component({
  selector: 'app-data-grid',
  templateUrl: './data-grid.component.html',
  styleUrls: ['./data-grid.component.less']

})
export class DataGridComponent implements OnInit {

  @Input() insight: insight;
  public displayTemplate: InsightTemplates;
  public MyDidYouKnow: MyDidYouKnow[];
  public isDisplay:boolean = true;


  constructor(insightService: InsightService, private getResourcesService: GetResourcesService) {

  }
  ngOnInit() { }
  displayInsight(templateType): boolean {
    if (!this.getResourcesService.MetaData || !this.getResourcesService.MetaData.InsightTemplates)
      return false;

    let result = this.getResourcesService.MetaData.InsightTemplates
      .filter(x => x.InsightType === this.insight.InsightType)
    this.displayTemplate = result[0];
    return result[0].TemplateType === templateType;
  }
  displayDidUKnow(templateType): boolean {
  
    if (!this.getResourcesService.MetaData || !this.getResourcesService.MetaData.InsightTemplates)
      return false;
    let result = this.getResourcesService.MetaData.InsightTemplates
      .filter(x => x.MyDidYouKnow && x.MyDidYouKnow.length > 0 && x.TemplateType == templateType)
    let r = result.filter(x => x.InsightType == this.insight.InsightType);
    if (r.length > 0)
      this.MyDidYouKnow = r[0].MyDidYouKnow;
    
      let currencyCode = this.insight.CurrancyCode;
    let didUKnow = this.MyDidYouKnow.filter(x => x.CurrancyCode === currencyCode);
    if (didUKnow.length == 0) {
      this.isDisplay = false
    }   
    else{
      this.MyDidYouKnow = didUKnow;     
    }
    return result.length > 0 && result[0].TemplateType === templateType;
  }


}
