import { Component, OnInit ,Input} from '@angular/core';
import { GetResourcesService } from '../services/get-resources.service';
import { didUKnowType,insight, DidYouKnow,MyDidYouKnow } from '../../models';
import { parse } from 'querystring';


@Component({
  selector: 'app-did-you-know',
  templateUrl: './did-you-know.component.html',
  styleUrls: ['./did-you-know.component.less']
})
export class DidYouKnowComponent implements OnInit {

  
  private didUKnowType:didUKnowType;
  public randomStatement:DidYouKnow = <DidYouKnow>{};
  public randomStatements:DidYouKnow[]=[]
  @Input() insight:insight;
  @Input() MyDidYouKnow:MyDidYouKnow[];
  

  constructor(public getResourcesService:GetResourcesService) { 
    this.randomStatements = [];
  
  }
  ngOnInit(){
   
   if(this.MyDidYouKnow)
   {
      for(var i=0;i<this.MyDidYouKnow.length;i++)
      {
          var didYouKnowItem:DidYouKnow[];
          didYouKnowItem = this.getResourcesService.MetaData.DidYouKnow.filter(x=> parseInt(x.ID)
          === this.MyDidYouKnow[i].DYKId);
          if(didYouKnowItem.length>0)
          {
            this.randomStatements.push(didYouKnowItem[0])
          }
      }
      if(this.randomStatements.length>0)
      {
        this.selectRandom(this.randomStatements);
      }
   }
   
  }  
  selectRandom(statements:DidYouKnow[])
  {
        
      var item = statements[Math.floor(Math.random()*statements.length)];  
      this.randomStatement = item;
  }

}
