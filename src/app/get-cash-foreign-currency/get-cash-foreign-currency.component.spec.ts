import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetCashForeignCurrencyComponent } from './get-cash-foreign-currency.component';

describe('GetCashForeignCurrencyComponent', () => {
  let component: GetCashForeignCurrencyComponent;
  let fixture: ComponentFixture<GetCashForeignCurrencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetCashForeignCurrencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetCashForeignCurrencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
