import { Component, OnInit,Input } from '@angular/core';
import { insight ,InsightTemplates,InsightDetails} from  '../../models';
import { GetResourcesService } from '../services/get-resources.service';
import { InsightService } from '../services/insight.service';

@Component({
  selector: 'app-get-cash-foreign-currency',
  templateUrl: './get-cash-foreign-currency.component.html',
  styleUrls: ['./get-cash-foreign-currency.component.less']
})
export class GetCashForeignCurrencyComponent implements OnInit {

  @Input() insight:insight;
  @Input() insightTemplates:InsightTemplates;  
  public displayModal :boolean = false;
  public _insightService:InsightService;
  
  constructor(public getResourcesService:GetResourcesService,public insightService:InsightService) { }
 
  ngOnInit() {
    this._insightService = this.insightService;
  }
  
  showModal(){
    
    this.displayModal = true;
    
  
  } 

}
