import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsightDateComponent } from './insight-date.component';

describe('InsightDateComponent', () => {
  let component: InsightDateComponent;
  let fixture: ComponentFixture<InsightDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsightDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsightDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
