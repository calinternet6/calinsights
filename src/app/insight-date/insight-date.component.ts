import { Component, OnInit,Input } from '@angular/core';
import { insight } from  '../../models';

@Component({
  selector: 'app-insight-date',
  templateUrl: './insight-date.component.html',
  styleUrls: ['./insight-date.component.less']
})
export class InsightDateComponent implements OnInit {

  @Input() insight: insight;
  constructor() { }

  ngOnInit() {
    console.log(this.insight.TransactionDate);
  }

}
