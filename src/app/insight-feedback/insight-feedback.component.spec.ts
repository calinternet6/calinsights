import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsightFeedbackComponent } from './insight-feedback.component';

describe('InsightFeedbackComponent', () => {
  let component: InsightFeedbackComponent;
  let fixture: ComponentFixture<InsightFeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsightFeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsightFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
