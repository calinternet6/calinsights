import { Component, OnInit, Input } from '@angular/core';
import { insight, UpdateFeedbackRequest } from '../../models';
import { InsightService } from '../services/insight.service';
import { tryParse } from 'selenium-webdriver/http';

@Component({
  selector: 'app-insight-feedback',
  templateUrl: './insight-feedback.component.html',
  styleUrls: ['./insight-feedback.component.less']
})
export class InsightFeedbackComponent implements OnInit {

  @Input() insight: insight;
  public UpdateFeedbackRequest: UpdateFeedbackRequest;
  public changeFeedbackStyle: number = 0;
  constructor(public insightService: InsightService) { }

  ngOnInit() {
    this.changeFeedbackStyle = this.insight.InsightFeedback
  }

  setFeedback(feedbackType: number) {

    this.UpdateFeedbackRequest = new UpdateFeedbackRequest();
    this.UpdateFeedbackRequest.ID = parseInt(this.insight.ID);

    if (this.insight.InsightFeedback === feedbackType) {
          feedbackType = 0;          
    }

    this.UpdateFeedbackRequest.InsightFeedback = feedbackType;
    this.insight.InsightFeedback = this.UpdateFeedbackRequest.InsightFeedback;
    this.UpdateFeedbackRequest.InsightId = this.insight.InsightId;
    this.UpdateFeedbackRequest.IsExtraInfoDisplayed = null;
    
   
    this.insightService.setFeedback(this.UpdateFeedbackRequest).subscribe((response) => {      

      setTimeout(()=>{
        this.insightService.displayRequestSpinner = false;
      },250);
     
      if (feedbackType == 0) {
        this.changeFeedbackStyle = 0; //NoneResponse
      }
      else if (feedbackType == 1) {
        this.changeFeedbackStyle = 1; //Like
      }
      else {
        this.changeFeedbackStyle = 2; //Unlike
      }
    })
  }


}
