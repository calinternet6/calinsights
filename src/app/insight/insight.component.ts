import { Component, OnInit,AfterViewInit,EventEmitter,Input,Output  } from '@angular/core';
import {  insightType, insight,InsightTemplates } from '../../models';
import { InsightService } from '../services/insight.service';



@Component({
  selector: 'app-insight',
  templateUrl: './insight.component.html',
  styleUrls: ['./insight.component.less']
})
export class InsightComponent implements AfterViewInit {

  public insights:insight[];
  public data;
  public user;
  public spinnerInsight:boolean = true;
  public _insightService:InsightService
  @Input() insight:insight;
  @Input() insightTemplates:InsightTemplates;  

  constructor(insightService:InsightService) { 
  
    this.insights = [];
    this._insightService = insightService;
    
  
    this._insightService.getInsight().subscribe((data)=>{      
      this.spinnerInsight = false;
      if(data.Insights)
      {    
        console.log("data.Insights",data.Insights)
       this.insights = data.Insights;
       this._insightService.fillInsights(this.insights);
       this._insightService.fillCustomerName(data.CustomerName);
       this.user = data.CustomerName.FirstName;      
       
      }
      else
      {
        this.insights = [];    
      }      
    });   
  }

  ngAfterViewInit() {

   
  }
  ngOnInit() {    
             
  }

 
}
