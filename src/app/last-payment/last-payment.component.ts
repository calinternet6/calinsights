import { Component, OnInit,Output,EventEmitter,Input } from '@angular/core';  
import { insight, insightType,MetaData,InsightTemplates } from '../../models';
import { GetResourcesService } from '../services/get-resources.service';
import { Observable } from 'rxjs/Observable';
import { filter } from 'rxjs/internal/operators/filter';


@Component({
  selector: 'app-last-payment',
  templateUrl: './last-payment.component.html',
  styleUrls: ['./last-payment.component.less']
})
export class LastPaymentComponent implements OnInit {

  @Input() insight:insight;
  @Input() insightTemplates:InsightTemplates;
  //public insightTemplates:InsightTemplates;
   
 
  constructor(public getResourcesService:GetResourcesService) {
  
  
    this.insightTemplates = this.getResourcesService.MetaData.InsightTemplates.filter(x=>x.InsightType == 1)[0]
    
   }

  ngOnInit() {   
    //var ttt = this.getCurrencySymbolById.getCurrencyByName('dollar')
   
  } 

}
