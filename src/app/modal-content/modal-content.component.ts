import { Component, OnInit ,Input,Output,EventEmitter,OnChanges} from '@angular/core';
import {LastPaymentComponent } from '../last-payment/last-payment.component';
import { InsightDetails,ModalInsightDetails} from  '../../models';
import { groupBy } from 'rxjs/internal/operators/groupBy';
import { tryParse } from 'selenium-webdriver/http';
import * as moment from 'moment';
import { GetResourcesService } from '../services/get-resources.service';

@Component({
  selector: 'app-modal-content',
  templateUrl: './modal-content.component.html',
  styleUrls: ['./modal-content.component.less']
})
export class ModalContentComponent implements OnInit {
//https://stackblitz.com/edit/angular-communication-1?file=app%2Fside-bar%2Fside-bar.component.ts

 

  @Input() displayModal:boolean;   
  @Input() insightDetails:InsightDetails[]; 
  @Input() insightTitle:string;
  @Output() displayModalChange = new EventEmitter();
  public firstCurrencyCode:number;
  public modalInsightDetails:ModalInsightDetails[]=[];
  constructor(public getResourcesService:GetResourcesService) { 
   
  }
  ngOnChanges()
  {
  
  }
  ngOnInit() {
  
    var result = this.insightDetails.reduce((r, a)=> {
    
      r[a.CardLast4Digits] = r[a.CardLast4Digits] || [];
      r[a.CardLast4Digits].push(a);   
      return r;
     }, Object.create(null));           
    for (var key in result) {                    
          
          let item = new ModalInsightDetails();
          item.CardLast4Digits = key;
          item.InsightDetails = result[key]
          this.firstCurrencyCode = item.InsightDetails[0].CurrancyCode;
          item.total = this.calcTotal(item.InsightDetails);
          this.modalInsightDetails.push(item); 
          
         
      }
      
  }
  calcTotal(items:InsightDetails[])
  {
    let total = 0;
    for(var i=0;i<items.length;i++)
    {
      total = total + items[i].TransactionSum;
    }
    return total;//איזה מטבע להציג אם יש כמה מטבעות
  }
  removeModal(){
    this.displayModal = false;
    this.displayModalChange.emit(this.displayModal);
    
  }

}

