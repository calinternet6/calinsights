import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDirectDebitComponent } from './new-direct-debit.component';

describe('NewDirectDebitComponent', () => {
  let component: NewDirectDebitComponent;
  let fixture: ComponentFixture<NewDirectDebitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewDirectDebitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewDirectDebitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
