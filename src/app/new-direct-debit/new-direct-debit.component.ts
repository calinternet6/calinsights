import { Component, OnInit ,Input} from '@angular/core';
import { insight, insightType ,InsightTemplates} from '../../models';
import { GetResourcesService } from '../services/get-resources.service';

@Component({
  selector: 'app-new-direct-debit',
  templateUrl: './new-direct-debit.component.html',
  styleUrls: ['./new-direct-debit.component.less']
})
export class NewDirectDebitComponent implements OnInit {

  @Input() insight:insight;
  @Input() insightTemplates:InsightTemplates;
  constructor(public getResourcesService:GetResourcesService) { }

  ngOnInit() {
  }

}
