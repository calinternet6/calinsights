import { Component, OnInit ,Input} from '@angular/core';
import { insight, insightType,InsightTemplates } from '../../models';
import { GetResourcesService } from '../services/get-resources.service';

@Component({
  selector: 'app-refund',
  templateUrl: './refund.component.html',
  styleUrls: ['./refund.component.less']
})
export class RefundComponent implements OnInit {

  @Input() insight:insight;
  @Input() insightTemplates:InsightTemplates;
  constructor(public getResourcesService:GetResourcesService) { }

  ngOnInit() {
  }
 
}
