import { TestBed, inject } from '@angular/core/testing';

import { GetResourcesService } from './get-resources.service';

describe('GetResourcesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetResourcesService]
    });
  });

  it('should be created', inject([GetResourcesService], (service: GetResourcesService) => {
    expect(service).toBeTruthy();
  }));
});
