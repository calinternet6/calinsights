import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';
import { currency,InsightTemplates,MetaData,DidYouKnow } from '../../models';
import * as _ from 'lodash';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import { environment } from '../../../src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class GetResourcesService {

  public coins:currency[] = [];
  public MetaData:MetaData = new MetaData;

  constructor(private http: HttpClient) { }

  public getDidYouKnowJSON() {
   return this.MetaData.DidYouKnow;
  }
  public loadCoins():Observable<any>
  {    
     return this.http.get("src/resources/currency.json")
  }

  public getMetaData():Observable<any>
  {  
  
    return this.http.get(environment.baseUrl +"Cal4U-Insights/CalInsights/api/GetInsightsMetaData");
  }
  public getCurrencySymbolById(id:string)
  {
    
 
             
  }
  public getSymbol(id:number):string
  {
    for(let i=0 ; i<this.coins.length;i++)
    {
     
        if( this.coins[i].name == id.toString())
        return this.coins[i].symbol;
    }
    return '';
  
  }
  public fillMetaData(data:any)
  {
    this.MetaData.DidYouKnow = [];
    this.MetaData.DidYouKnow = data.DidYouKnow;
    this.MetaData.InsightTemplates = [];
    this.MetaData.InsightTemplates = data.InsightTemplates;   
    
    console.log("YESH GAM METADATA")
  }
  public fillCoinsArr(data:any)
  {
 
    for (var key in data) {
      if (data.hasOwnProperty(key)) {  
        var c = new currency();
        c.name = key;
        c.symbol = data[key];      
          this.coins.push(c);
      }
    }
    
  }
}
