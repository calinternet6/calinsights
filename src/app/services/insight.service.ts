import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { insight, insightType,CustomerName ,UpdateFeedbackRequest} from '../../models';
import * as moment from 'moment';
import { environment } from '../../../src/environments/environment';
//import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})

export class InsightService {

  public insights:insight[];
  public displayRequestSpinner:boolean = false;
  public CustomerName:CustomerName;
  constructor(private http: HttpClient) { }
  public fillInsights(insightsList:insight[])
  {
    this.insights = insightsList;
  }
  public fillCustomerName(CustomerName:CustomerName)
  {
    this.CustomerName = CustomerName;
    
  }
  public getInsight() : Observable<any>{
 
   
    console.log("GET INSIGHTS")
    //return this.http.get("https://stage-restservices.cal-online.co.il/Cal4U-Insights/CalInsights/api/GetCustInsights?fromDate");   
   return this.http.get(environment.baseUrl + "Cal4U-Insights/CalInsights/api/GetCustInsights?fromDate=2018-01-01");
   
  
}
getCashMonth(insight:insight)
  {   
     
    if(insight.InsightDetails)
    {
      if(insight.InsightDetails.length>0)
        {                
          let date = insight.InsightDetails[0].TransactionDate;
          var insightDueDate = moment(date, 'dd/MM/yyyy').format('MM/YYYY');  
          return insightDueDate;      
          //return insightDueDate.getMonth()+1 +'/'+ insightDueDate.getFullYear();
        }
    }
  }
  setFeedback(UpdateFeedbackRequest:UpdateFeedbackRequest):Observable<any>{
    this.displayRequestSpinner = true;     
    return this.http.post(environment.baseUrl + "Cal4U-Insights/CalInsights/api/UpdateFeedback",UpdateFeedbackRequest)
  
  }

  writeToAms(){}

}

