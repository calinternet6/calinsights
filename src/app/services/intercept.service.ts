import { Injectable } from '@angular/core';
import {InsightService} from '../services/insight.service';
import {
  HttpEvent, 
  HttpInterceptor, 
  HttpHandler, 
  HttpRequest,
  HttpResponse
} from '@angular/common/http'
import { Observable } from 'rxjs';
import { catchError,tap} from 'rxjs/operators';

@Injectable()//{providedIn: 'root'}

export class InterceptService  implements HttpInterceptor {

	private totalRequests = 0;
	constructor(private insightService: InsightService) { }


	
	// intercept request and add token
  	intercept(request: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>> {
	
		//	this.route.queryParams.subscribe(params => {
    //    this.param1 = params['param1'];
     //   this.param2 = params['param2'];
    //});

    	// modify request
	    //request = request.clone({
	    //  setHeaders: {
			//		Authorization: `Bearer ${localStorage.getItem('authToken')}`
			//		X-Site-Id: `Bearer ${localStorage.getItem('siteId')}`
				//Authorization:'Bearer CALAuthScheme nSnvq8TdKRvBw1joYZciTX6exPccN5jJIu8lFjiBftOfzJnMyNtj14sr/vTwFmw84EON1jOWAib7Yx++4WqT2TQGHHVp7wx+7iaz++WaTH5by2Y27rXVuBI767nvRUG31Ny/qe3NNSbCJLQjQwKW7K+PJ41Vd0imjSdxXmAEIbiVofTZaGd2zi7+zEb3LiFanCXLmaeLIHEhmADJBgkFY+QZQorfh0DltQuN/TNlRpiDQvJz4PpDSamhXe2RGWkYAnlahX/Iq61I4uzQhHIzl6MfaGzRLYfGA6wNClwjusevwlhuyAhgQysmJV0l8/l2m+dUIKWEqHDIGRR3g8aXMw=='
			//X-Site-Id:''
			//Content-Type:'application/json'
			//	}
			//});
			
			request = request.clone({
				setHeaders: {
					'Content-Type':  'application/json',
					'Authorization': 'CALAuthScheme nSnvq8TdKRvBw1joYZciTX6exPccN5jJIu8lFjiBftOfzJnMyNtj14sr/vTwFmw84EON1jOWAib7Yx++4WqT2TQGHHVp7wx+7iaz++WaTH5by2Y27rXVuBI767nvRUG31Ny/qe3NNSbCJLQjQwKW7K+PJ41Vd0imjSdxXmAEIbiVofTZaGd2zi7+zEb3LiFanCXLmaeLIHEhmADJBgkFY+QZQorfh0DltQuN/TNlRpiDQvJz4PpDSamhXe2RGWkYAnlahX/Iq61I4uzQhHIzl6MfaGzRLYfGA6wNClwjusevwlhuyAhgQysmJV0l8/l2m+dUIKWEqHDIGRR3g8aXMw==',
					'X-Site-Id':'05D905EB-810A-4680-9B23-1A2AC46533BF'
				}
			});
					 
			
			
	    return next.handle(request)
	    .pipe(
	        tap(event => {
	          if (event instanceof HttpResponse) {
	             
							
	            // http response status code
	         
	          }
	        }, error => {
	   			// http response status code
	         
	          	console.error("status code:");
	          	console.error(error.status);
	          	console.error(error.message);
	         

	        })
	      )

    };
  
 
}
