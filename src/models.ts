export class MetaData
{
    DidYouKnow:DidYouKnow[];
    InsightTemplates:InsightTemplates[];


}
export class InsightTemplates{
    TemplateType:number;
    InsightType:number;
    Title:string;
    Description:string;
    MyDidYouKnow:MyDidYouKnow[];
    
}
export class MyDidYouKnow
{
    DYKId:number;
    CurrancyCode:number;
}
export class DidYouKnow
{
    ID:string;
    Title:string;
    Description:string;
}
export class insight {
  
    ID:string;
    InsightId:number;
    InsightType:number;
    InsightFeedback:number;
    TemplateType:number;
    CardDebitDate:string;
    CustIdType:string;
    CustId:string;
    CardLast4Digits:string;
    CardIntId:string;
    MerchantName:string;
    MerchantBranch:string;
    TransactionSum:string;
    TransactionDate:Date;
    PercentageChange:number;
    TotalSumNis:number;
    TotalSumForeignCurrancy:number;
    CurrancyCode:number;
    CurrentInstallmentNum:number;
    TotalInstallmentsNum:number;
    InsightDetails:InsightDetails[];
    InsertDate:string;

}
export class CustomerName{
    FirstName:string;
    LastName:string;
}
export class ModalInsightDetails
{
    CardLast4Digits:string;
    InsightDetails:InsightDetails[];
    total:number;
}
export class InsightDetails
{
    CardLast4Digits:string;
    CardIntId:string;
    MerchantName:string;
    TransactionSum:number;
    TransactionDate:Date;
    CurrancyCode:number;
}
export class column{
    isBlue:boolean;
    value:string;
    date:Date;
    currencyCode:number;
    displayDate:string;
    height:string;
}
export class mock{
    date:string;
    value:string;
}
export enum insightType{      
    One = 1,
    Two = 2,
    Three = 3,
    Four = 4,
    Five = 5,
    Six = 6,
    Seven = 7

}
export enum didUKnowType{
    Dollar,
    Euro,
    GetLocalCashCurrency
}
export class currency
{
    name:string;
    symbol:string;
}
export class UpdateFeedbackRequest
{
     ID:number;
     InsightId:number;
     InsightType:number;
     InsightFeedback:InsightFeedback;
     IsExtraInfoDisplayed:boolean;

}
export enum InsightFeedback
{
    NoneResponse = 0,
    Like = 1,
    Unlike = 2
}